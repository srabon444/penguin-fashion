# Penguin Fashion

A simple E Commerce Website for Womens Jacket

## [Live Application](https://srabon444.gitlab.io/penguin-fashion/)


## Features

- UI shows Womens Jacket e-commerce Website

## Tech Stack

- Bootstrap
- HTML
- CSS

**Hosting:**
- Gitlab Pages


